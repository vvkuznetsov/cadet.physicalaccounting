﻿namespace Cadet.PhysicalAccounting.DBProxy.Extensions
{
    public class BaseRazorModel
    {
        public string SuccessMessage = null;
        public string InfoMessage = null;
        public string ErrorMessage = null;
    }
}