﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using Cadet.PhysicalAccounting.DBProxy.Extensions;
using System.Data.Entity;

namespace Cadet.PhysicalAccounting.DBProxy
{

    #region Context

    public partial class DBContext
    {
        //public IQueryable<Cadet> Search(string substring)
        //{
        //    var substrings = substring.ToLower().Split(new string[] {" "}, StringSplitOptions.RemoveEmptyEntries);

        //    return Cadets.Where(cadet => SearchCompanies(substrings).Compile()(cadet));
        //}



        // don't work
        // call return Cadets.Where(SearchCompanies(substrings));
    //public static Expression<Func<Cadet, bool>> SearchCompanies(params string[] keywords)
    //{
    //    var predicate = PredicateBuilder.False<Cadet>();
        
    //    foreach (string keyword in keywords)
    //    {
    //        string temp = keyword;
    //        predicate = predicate.Or(p => p.LastName.Contains(temp));
    //        //predicate = predicate.Or(p => p.FirstName.Contains(temp));
    //        //predicate = predicate.Or(p => p.FathersName.Contains(temp));
    //        //predicate = predicate.Or(p => p.Group.ToString() == temp);
    //    }

    //    return predicate;
    //}


    private static bool Predicat(string[] substrings, Cadet cadet)
        {
            var sb = new StringBuilder();

            sb.Append(cadet.LastName.ToLower());
            sb.Append(cadet.FirstName.ToLower());
            sb.Append(cadet.FathersName.ToLower());
            sb.Append(cadet.Group.Number);

            var line = sb.ToString();

            return Predicat(line, substrings);
        }

        private static bool Predicat(string line, string[] substrings)
        {
            return substrings.All(line.Contains);
        }

        [Obsolete]
        private static bool Predicat(string substring, Cadet cadet)
        {
            return cadet.LastName.ToLower().Contains(substring) ||
                   cadet.FirstName.ToLower().Contains(substring) ||
                   cadet.FathersName.ToLower().Contains(substring) ||
                   cadet.Group.Number.ToString() == substring;
        }
    }

    #endregion


    #region Entities

    public partial class Cadet
    {
        public BaseRazorModel Info = new BaseRazorModel();
    }
    public partial class Course
    {
        public BaseRazorModel Info = new BaseRazorModel();
    }
    public partial class Faculty
    {
        public BaseRazorModel Info = new BaseRazorModel();
    }
    public partial class Group
    {
        public BaseRazorModel Info = new BaseRazorModel();
        private string _compiledGroupNumber;

        public void CalcCompiledGroupNumber()
        {
            if (Course != null && Course.Faculty != null)
            {
                _compiledGroupNumber = string.Format("{0}{1}{2}", Course.Faculty.Number, Course.Number, Number);
            }
            else
            {
                _compiledGroupNumber = Number.ToString();
            }     
        }
        public string CompiledGroupNumber
        {
            get
            {
                if (_compiledGroupNumber == null)
                {
                    CalcCompiledGroupNumber();
                }

                return _compiledGroupNumber;
            }
        }
    }
    public partial class Norm
    {
        public BaseRazorModel Info = new BaseRazorModel();
    }
    public partial class NormType
    {
        public BaseRazorModel Info = new BaseRazorModel();
    }
    public partial class Rank
    {
        public BaseRazorModel Info = new BaseRazorModel();
    }
    public partial class Result
    {
        public BaseRazorModel Info = new BaseRazorModel();
    }
    public partial class Semester
    {
        public BaseRazorModel Info = new BaseRazorModel();
    }

    #endregion

}
