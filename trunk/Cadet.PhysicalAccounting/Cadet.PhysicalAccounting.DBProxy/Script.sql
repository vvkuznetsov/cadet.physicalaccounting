﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
USE [Cadet.PhysicalAccounting];
GO

IF OBJECT_ID(N'[dbo].[CHK_Quarter]') IS NOT NULL
    ALTER TABLE [dbo].[OfficerResults] DROP CONSTRAINT [CHK_Quarter];
GO

ALTER TABLE dbo.OfficerResults
   ADD CONSTRAINT CHK_Quarter
   CHECK (Quarter >= 1 AND Quarter <= 4);
GO

IF OBJECT_ID(N'[dbo].[Search]') IS NOT NULL
    DROP PROCEDURE [Search];
GO

CREATE PROCEDURE Search
	@line varchar(MAX)
AS
BEGIN
	Declare @arg varchar(MAX) = @line
	Declare @substring varchar(20) = null
	CREATE TABLE #Substrings(String varchar(MAX))

	WHILE LEN(@arg) > 0
	BEGIN
		IF PATINDEX('% %',@arg) > 0
		BEGIN
			SET @substring = SUBSTRING(@arg, 0, PATINDEX('% %',@arg))
			
			IF (LEN(@substring + ' ')>0)
			BEGIN
				INSERT INTO #Substrings SELECT '%' + @substring + '%'
			END

			SET @arg = SUBSTRING(@arg, LEN(@substring + ' ') + 2, LEN(@arg))

		END
		ELSE
		BEGIN
			SET @substring = @arg
			SET @arg = NULL
			INSERT INTO #Substrings SELECT '%' + @substring + '%'
		END
	END

	DECLARE @count AS int;
	select @count = COUNT(*) from #Substrings


	PRINT 'count = ' + CAST(@count as varchar(50))

	

	--CREATE TABLE #Temp (_count int)
	--INSERT INTO #TEMP (_count)
	--	SELECT
	--	(
	--			SELECT COUNT(*) As 'One' FROM #Substrings
	--			WHERE Cadets.LastName+Cadets.FirstName+Cadets.FathersName+CAST(Groups.Number as VARCHAR(50)) LIKE String
	--	) AS _count
	--	FROM Cadets
	--	JOIN Groups ON GroupId = Groups.Id


	--SELECT * FROM #Temp
	--WHERE _count = 2

	SELECT 
		Cadets.Id,
		Cadets.LastName,
		Cadets.FirstName,
		Cadets.FathersName,
		Cadets.RankId,
		Cadets.Photo,
		Cadets.[Year],
		Cadets.GroupId
	FROM Cadets
	JOIN Groups ON GroupId = Groups.Id
	JOIN Courses ON CourseId = Courses.Id
	JOIN Faculties ON FacultyId = Faculties.Id
	WHERE 
		@count = ALL
		(
			SELECT COUNT(*) FROM #Substrings
			WHERE Cadets.LastName + Cadets.FirstName + Cadets.FathersName
			+ CAST(Faculties.Number as VARCHAR(50))
			+ CAST(Courses.Number as VARCHAR(50))
			+ CAST(Groups.Number as VARCHAR(50)) LIKE String
		)

	--SELECT * FROM #Substrings
END
GO