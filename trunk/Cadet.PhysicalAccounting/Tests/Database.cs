﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Cadet.PhysicalAccounting.DBProxy;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Tests
{
    /// <summary>
    /// Summary description for Database
    /// </summary>
    [TestClass]
    public class Database
    {
        public Database()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        [TestMethod]
        public void TestMethod1()
        {
            using (var context = new DBContext())
            {
                Assert.IsNotNull(context);
            }
        }

        [TestMethod]
        public void Search1()
        {
            using (var context = new DBContext())
            {
                var result1 = context.Search("Иван").ToList();
                var result2 = context.Search("иван").ToList();
                var result3 = context.Search("ИВАН").ToList();
                var result4 = context.Search("иВАН").ToList();
                var result5 = context.Search("Ив").ToList();
                var result6 = context.Search("ван").ToList();
                var result7 = context.Search("ван").ToList();

                Assert.IsTrue(Compare(result1, result2));
                Assert.IsTrue(Compare(result2, result3));
                Assert.IsTrue(Compare(result3, result4));
                Assert.IsTrue(Compare(result4, result5));
                Assert.IsTrue(Compare(result5, result6));
                Assert.IsTrue(Compare(result6, result7));
            }
        }

        [TestMethod]
        public void Search2()
        {
            using (var context = new DBContext())
            {
                var result1 = context.Search("Иван").ToList();
                var result2 = context.Search("иван анович").ToList();

                Assert.IsTrue(Compare(result1, result2));
            }
        }

        [TestMethod]
        public void Search3()
        {
            using (var context = new DBContext())
            {
                var result1 = context.Search("ИВАН 1").ToList();
                var result2 = context.Search("1 иВАН").ToList();
                var result3 = context.Search("Ив 1").ToList();
                var result4 = context.Search("1 ван ив").ToList();
                var result5 = context.Search("ван 1 и").ToList();

                Assert.IsTrue(Compare(result1, result2));
                Assert.IsTrue(Compare(result2, result3));
                Assert.IsTrue(Compare(result3, result4));
                Assert.IsTrue(Compare(result4, result5));
            }
        }

        [TestMethod]
        public void Search4()
        {
            IEnumerable<Cadet.PhysicalAccounting.DBProxy.Cadet> cadets;

            using (var context = new DBContext())
            {
                var search = context.Search("Иван").ToList();

                cadets = search.Join(
                    context.Ranks,
                    cadet => cadet.RankId,
                    rank => rank.Id,
                    (cadet, rank) =>
                    {
                        cadet.Rank = rank;
                        return cadet;
                    }
                    ).ToList();

                Assert.IsNotNull(search);
            }

            var cadet1 = cadets.Take(1).Single();
            Assert.IsNotNull(cadet1.Rank.Id);
        }

        private static bool Compare(List<Cadet.PhysicalAccounting.DBProxy.Cadet> arg1, List<Cadet.PhysicalAccounting.DBProxy.Cadet> arg2)
        {
            if (arg1.Count != arg2.Count)
                return false;

            return arg1.All(cadet => arg2.Any(c => Predicat(c, cadet)));
        }

        private static bool Predicat(Cadet.PhysicalAccounting.DBProxy.Cadet arg1, Cadet.PhysicalAccounting.DBProxy.Cadet arg2)
        {
            return arg1.LastName == arg2.LastName &&
                arg1.FirstName == arg2.FirstName &&
                arg1.FathersName == arg2.FathersName &&
                arg1.Group.Number == arg2.Group.Number;
        }
    }
}
