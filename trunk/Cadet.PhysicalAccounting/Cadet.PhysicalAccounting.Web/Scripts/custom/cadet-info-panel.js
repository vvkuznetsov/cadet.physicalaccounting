﻿$(document).ready(function () {

    $('#save-photo-button').click(function (event) {
        if ($('#imgfile').val() == "") {
            $('#imgfile').click();
            event.preventDefault();
        }
    });

    $("#imgfile").change(function () {
        if ($('#imgfile').val() != "") {
            var btn = $("#save-photo-button");
            btn.html('<i class="icon-ok  icon-white"></i>Сохранить фото');
            btn.attr("type", "submit");
            btn.addClass('btn-success');
            btn.css("visibility", "visible");
        }
    });

});
