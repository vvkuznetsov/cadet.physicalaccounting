﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Cadet.PhysicalAccounting.DBProxy;

namespace Cadet.PhysicalAccounting.Web.Controllers
{
    public class NormController : Controller
    {
        private DBContext db = new DBContext();

        //
        // GET: /Norm/

        public ActionResult Index()
        {
            if (!User.IsPriveleged())
            {
                return View("AccessDenied");
            }

            var norms = db.Norms.Include("NormType");
            return View(norms.ToList().OrderBy(x=>x.NormTypeId).ThenBy(x=>x.Name));
        }

        //
        // GET: /Norm/Create

        public ActionResult Create()
        {
            if (!User.IsPriveleged())
            {
                return View("AccessDenied");
            }

            ViewBag.NormTypeId = new SelectList(db.NormTypes, "Id", "Name");
            return View();
        }

        //
        // POST: /Norm/Create

        [HttpPost]
        public ActionResult Create(Norm norm)
        {
            if (!User.IsPriveleged())
            {
                return View("AccessDenied");
            }

            if (ModelState.IsValid)
            {
                db.Norms.AddObject(norm);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.NormTypeId = new SelectList(db.NormTypes, "Id", "Name", norm.NormTypeId);
            return View(norm);
        }

        //
        // GET: /Norm/Edit/5

        public ActionResult Edit(int id = 0)
        {
            if (!User.IsPriveleged())
            {
                return View("AccessDenied");
            }

            Norm norm = db.Norms.Single(n => n.Id == id);
            if (norm == null)
            {
                return HttpNotFound();
            }
            ViewBag.NormTypeId = new SelectList(db.NormTypes, "Id", "Name", norm.NormTypeId);
            return View(norm);
        }

        //
        // POST: /Norm/Edit/5

        [HttpPost]
        public ActionResult Edit(Norm norm)
        {
            if (!User.IsPriveleged())
            {
                return View("AccessDenied");
            }

            if (ModelState.IsValid)
            {
                db.Norms.Attach(norm);
                db.ObjectStateManager.ChangeObjectState(norm, EntityState.Modified);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.NormTypeId = new SelectList(db.NormTypes, "Id", "Name", norm.NormTypeId);
            return View(norm);
        }

        //
        // GET: /Norm/Delete/5

        public ActionResult Delete(int id = 0)
        {
            if (!User.IsPriveleged())
            {
                return View("AccessDenied");
            }

            Norm norm = db.Norms.Single(n => n.Id == id);
            if (norm == null)
            {
                return HttpNotFound();
            }
            return View(norm);
        }

        //
        // POST: /Norm/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            if (!User.IsPriveleged())
            {
                return View("AccessDenied");
            }

            Norm norm = db.Norms.Single(n => n.Id == id);
            db.Norms.DeleteObject(norm);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}