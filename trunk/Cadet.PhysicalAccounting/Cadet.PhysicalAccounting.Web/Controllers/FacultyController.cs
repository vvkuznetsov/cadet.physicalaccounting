﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Cadet.PhysicalAccounting.DBProxy;

namespace Cadet.PhysicalAccounting.Web.Controllers
{
    public class FacultyController : Controller
    {
        private DBContext db = new DBContext();

        //
        // GET: /Faculty/

        public ActionResult Index()
        {
            if (!User.IsPriveleged())
            {
                return View("AccessDenied");
            }

            return View(db.Faculties.ToList().OrderBy(f=>f.Number));
        }

        //
        // GET: /Faculty/Create

        public ActionResult Create()
        {
            if (!User.IsPriveleged())
            {
                return View("AccessDenied");
            }

            return View();
        }

        //
        // POST: /Faculty/Create

        [HttpPost]
        public ActionResult Create(Faculty faculty)
        {
            if (!User.IsPriveleged())
            {
                return View("AccessDenied");
            }

            if (ModelState.IsValid)
            {
                db.Faculties.AddObject(faculty);
                db.SaveChanges();
                FacultyTreeMaker.RefreshFacultyTreeModel();
                return RedirectToAction("Index");
            }

            return View(faculty);
        }

        //
        // GET: /Faculty/Edit/5

        public ActionResult Edit(int id = 0)
        {
            if (!User.IsPriveleged())
            {
                return View("AccessDenied");
            }

            Faculty faculty = db.Faculties.Single(f => f.Id == id);
            if (faculty == null)
            {
                return HttpNotFound();
            }
            return View(faculty);
        }

        //
        // POST: /Faculty/Edit/5

        [HttpPost]
        public ActionResult Edit(Faculty faculty)
        {
            if (!User.IsPriveleged())
            {
                return View("AccessDenied");
            }

            if (ModelState.IsValid)
            {
                db.Faculties.Attach(faculty);
                db.ObjectStateManager.ChangeObjectState(faculty, EntityState.Modified);
                db.SaveChanges();
                FacultyTreeMaker.RefreshFacultyTreeModel();
                return RedirectToAction("Index");
            }
            return View(faculty);
        }

        //
        // GET: /Faculty/Delete/5

        public ActionResult Delete(int id = 0)
        {
            if (!User.IsPriveleged())
            {
                return View("AccessDenied");
            }

            Faculty faculty = db.Faculties.Single(f => f.Id == id);
            if (faculty == null)
            {
                return HttpNotFound();
            }
            return View(faculty);
        }

        //
        // POST: /Faculty/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            if (!User.IsPriveleged())
            {
                return View("AccessDenied");
            }

            Faculty faculty = db.Faculties.Single(f => f.Id == id);

            foreach (var course in faculty.Courses.ToList())
            {
                foreach (var group in course.Groups.ToList())
                {
                    foreach (var cadet in group.Cadets.ToList())
                    {
                        foreach (var result in cadet.Results.ToList())
                        {
                            db.Results.DeleteObject(result);
                        }
                        db.Cadets.DeleteObject(cadet);
                    }
                    db.Groups.DeleteObject(group);
                }
                db.Courses.DeleteObject(course);
            }
            db.Faculties.DeleteObject(faculty);

            db.SaveChanges();
            FacultyTreeMaker.RefreshFacultyTreeModel();

            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}