﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Cadet.PhysicalAccounting.DBProxy;

namespace Cadet.PhysicalAccounting.Web.Controllers
{
    public class ResultController : Controller
    {
        private DBContext db = new DBContext();

        //
        // GET: /Result/

        public ActionResult Index()
        {
            if (!User.IsPriveleged())
            {
                return View("AccessDenied");
            }

            var results = db.Results.Include("Cadet").Include("Semester").Include("Norm");
            return View(results.ToList());
        }

        //
        // GET: /Result/Details/5

        public ActionResult Details(int id = 0)
        {
            if (!User.IsPriveleged())
            {
                return View("AccessDenied");
            }

            Result result = db.Results.Single(r => r.Id == id);
            if (result == null)
            {
                return HttpNotFound();
            }
            return View(result);
        }

        //
        // GET: /Result/Create

        public ActionResult Create()
        {
            if (!User.IsPriveleged())
            {
                return View("AccessDenied");
            }

            ViewBag.CadetId = new SelectList(db.Cadets, "Id", "FirstName");
            ViewBag.SemesterId = new SelectList(db.Semesters, "Id", "Id");
            ViewBag.NormId = new SelectList(db.Norms, "Id", "Name");
            return View();
        }

        //
        // POST: /Result/Create

        [HttpPost]
        public ActionResult Create(Result result)
        {
            if (!User.IsPriveleged())
            {
                return View("AccessDenied");
            }

            if (ModelState.IsValid)
            {
                db.Results.AddObject(result);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.CadetId = new SelectList(db.Cadets, "Id", "FirstName", result.CadetId);
            ViewBag.SemesterId = new SelectList(db.Semesters, "Id", "Id", result.SemesterId);
            ViewBag.NormId = new SelectList(db.Norms, "Id", "Name", result.NormId);
            return View(result);
        }

        //
        // GET: /Result/Edit/5

        public ActionResult Edit(int id = 0)
        {
            if (!User.IsPriveleged())
            {
                return View("AccessDenied");
            }

            Result result = db.Results.Single(r => r.Id == id);
            if (result == null)
            {
                return HttpNotFound();
            }
            ViewBag.CadetId = new SelectList(db.Cadets, "Id", "FirstName", result.CadetId);
            ViewBag.SemesterId = new SelectList(db.Semesters, "Id", "Id", result.SemesterId);
            ViewBag.NormId = new SelectList(db.Norms, "Id", "Name", result.NormId);
            return View(result);
        }

        //
        // POST: /Result/Edit/5

        [HttpPost]
        public ActionResult Edit(Result result)
        {
            if (!User.IsPriveleged())
            {
                return View("AccessDenied");
            }

            if (ModelState.IsValid)
            {
                db.Results.Attach(result);
                db.ObjectStateManager.ChangeObjectState(result, EntityState.Modified);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.CadetId = new SelectList(db.Cadets, "Id", "FirstName", result.CadetId);
            ViewBag.SemesterId = new SelectList(db.Semesters, "Id", "Id", result.SemesterId);
            ViewBag.NormId = new SelectList(db.Norms, "Id", "Name", result.NormId);
            return View(result);
        }

        //
        // GET: /Result/Delete/5

        public ActionResult Delete(int id = 0)
        {
            if (!User.IsPriveleged())
            {
                return View("AccessDenied");
            }

            Result result = db.Results.Single(r => r.Id == id);
            if (result == null)
            {
                return HttpNotFound();
            }
            return View(result);
        }

        //
        // POST: /Result/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            if (!User.IsPriveleged())
            {
                return View("AccessDenied");
            }

            Result result = db.Results.Single(r => r.Id == id);
            db.Results.DeleteObject(result);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}