﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Cadet.PhysicalAccounting.Web.Models;
using Cadet.PhysicalAccounting.Web.Models.MockGenerators;

namespace Cadet.PhysicalAccounting.Web.Controllers
{
    public static class FacultyTreeMaker
    {

        static FacultyTreeMaker()
        {
            RefreshFacultyTreeModel();
        }

        public static void RefreshFacultyTreeModel(bool testMode = false)
        {
            FacultyTreeModel tree = new FacultyTreeModel();
            TreeItem root = tree.Root;          
            IEnumerable<DBProxy.Faculty> modelFaculty;
            if (testMode)
            {
                modelFaculty = FacultyGenerator.GetFacultyList();
            }
            else
            {
                DBProxy.DBContext model = new DBProxy.DBContext();
                modelFaculty = model.Faculties;
            }

            foreach (var fac in modelFaculty.OrderBy(f=>f.Number))
            {
                var currentFaculty = root.AddChild(fac.Id,fac.Number + " факультет");
                foreach (var course in fac.Courses.OrderBy(f => f.Number))
                {
                    var currentCourse = currentFaculty.AddChild(course.Id, course.Number.ToString() + " курс");
                    foreach (var group in course.Groups.OrderBy(f => f.Number))
                    {
                        currentCourse.AddChild(group.Id, group.CompiledGroupNumber + " группа");
                    }
                }
            }

            CurrentFacultyTree = tree;         
        }

        public static FacultyTreeModel CurrentFacultyTree {get; private set;}
    }
}