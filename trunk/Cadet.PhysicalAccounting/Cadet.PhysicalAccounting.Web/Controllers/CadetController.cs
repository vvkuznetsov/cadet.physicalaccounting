﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Drawing;
using System.Drawing.Imaging;   
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Cadet.PhysicalAccounting.DBProxy;
using Cadet.PhysicalAccounting.Web.Models.MockGenerators;

namespace Cadet.PhysicalAccounting.Web.Controllers
{
    public class CadetController : Controller
    {
        private DBContext db = new DBContext();

        //
        // GET: /Cadet/

        public ActionResult Index(int groupId)
        {
            if (!User.IsPriveleged())
            {
                return View("AccessDenied");
            }

            var cadets = db.Cadets.Where(c=>c.GroupId == groupId).Include("Group").Include("Rank");
            var group = db.Groups.First(g => g.Id == groupId);
            ViewBag.GroupNumber = group.CompiledGroupNumber;
            ViewBag.CourseId = group.CourseId;
            ViewBag.GroupId = groupId;
            return View(cadets.ToList().OrderBy(c => c.LastName + c.FirstName + c.FathersName));
        }

        //
        // GET: /Cadet/Details/5

        public ActionResult Details(int id = 0)
        {
            if (!User.IsPriveleged())
            {
                return View("AccessDenied");
            }

            var cadet = db.Cadets.Single(c => c.Id == id);
            if (cadet == null)
            {
                return HttpNotFound();
            }
            return View(cadet);
        }

        //
        // GET: /Cadet/Create

        public ActionResult Create(int courseId, int groupId)
        {
            if (!User.IsPriveleged())
            {
                return View("AccessDenied");
            }

            ViewBag.SelectedGroupId = groupId;
            ViewBag.GroupId = new SelectList(db.Groups.Where(g => g.CourseId == courseId), "Id", "CompiledGroupNumber", groupId);
            ViewBag.RankId = new SelectList(db.Ranks, "Id", "Name");
            return View();
        }

        //
        // POST: /Cadet/Create

        [HttpPost]
        public ActionResult Create(DBProxy.Cadet cadet)
        {
            if (!User.IsPriveleged())
            {
                return View("AccessDenied");
            }

            if (ModelState.IsValid)
            {
                db.Cadets.AddObject(cadet);
                db.SaveChanges();
                return RedirectToAction("Index", new { groupId = cadet.GroupId});
            }

            ViewBag.GroupId = new SelectList(db.Groups, "Id", "CompiledGroupNumber", cadet.GroupId);
            ViewBag.RankId = new SelectList(db.Ranks, "Id", "Name", cadet.RankId);
            return View(cadet);
        }

        //
        // GET: /Cadet/Edit/5

        public ActionResult Edit(int id = 0, string successMsg = null)
        {
            if (!User.IsPriveleged())
            {
                return View("AccessDenied");
            }

            var cadet = db.Cadets.Single(c => c.Id == id);
            if (cadet == null)
            {
                return HttpNotFound();
            }
            ViewBag.GroupId = new SelectList(db.Groups, "Id", "CompiledGroupNumber", cadet.GroupId);
            ViewBag.RankId = new SelectList(db.Ranks, "Id", "Name", cadet.RankId);
            cadet.Info.SuccessMessage = successMsg;
            return View(cadet);
        }

        //
        // POST: /Cadet/Edit/5

        [HttpPost]
        public ActionResult Edit(DBProxy.Cadet cadet)
        {
            if (!User.IsPriveleged())
            {
                return View("AccessDenied");
            }

            if (ModelState.IsValid)
            {
                db.Cadets.Attach(cadet);
                db.ObjectStateManager.ChangeObjectState(cadet, EntityState.Modified);
                db.SaveChanges();
                return RedirectToAction("Index", new {groupId = cadet.GroupId});
            }
            ViewBag.GroupId = new SelectList(db.Groups, "Id", "CompiledGroupNumber", cadet.GroupId);
            ViewBag.RankId = new SelectList(db.Ranks, "Id", "Name", cadet.RankId);
            return View(cadet);
        }

        [HttpPost]
        public ActionResult SavePhoto(int cadetId, HttpPostedFileBase imgfile)
        {
            var model = new DBProxy.Cadet();

            using (var ctx = new DBProxy.DBContext())
            {
                if (cadetId > 0)
                {
                    model = ctx.Cadets.Include("Rank")
                        .Include("Group")
                        .Include("Group.Course")
                        .Include("Group.Course.Faculty")
                        .Include("Results")
                        .Include("Results.Norm")
                        .Include("Results.Norm.NormType")
                        .Include("Results.Semester")
                        .SingleOrDefault(o => o.Id == cadetId);


                    if (model == null)
                    {
                        model.Info.ErrorMessage = "Курсант не найден";
                    }
                    else if (imgfile != null && imgfile.ContentLength > 0)
                    {
                        var imgStream = ImageHelper.Shrink(imgfile.InputStream, 85, 0, 500);
                        model.Photo = imgStream.ToArray();
                        ctx.SaveChanges();
                        Convert.ToBase64String(model.Photo);
                    }
                }
                else if (cadetId < 0)
                {
                    // Test data
                    model.Info.InfoMessage = "Отображены тестовые данные";

                    model = CadetsGenerator.GetCadet();
                }
                else
                {
                    model.Info.ErrorMessage = "Не указан идентификатор курсанта";
                }

                ctx.Detach(model);
                return RedirectToAction("Edit", new { @id = model.Id, @successMsg = "Фотография успешно загружена" });
            }
        }


        //
        // GET: /Cadet/Delete/5

        public ActionResult Delete(int id = 0)
        {
            if (!User.IsPriveleged())
            {
                return View("AccessDenied");
            }

            var cadet = db.Cadets.Single(c => c.Id == id);
            if (cadet == null)
            {
                return HttpNotFound();
            }
            return View(cadet);
        }

        //
        // POST: /Cadet/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            if (!User.IsPriveleged())
            {
                return View("AccessDenied");
            }

            var cadet = db.Cadets.Single(c => c.Id == id);
            int gId = cadet.GroupId;
            foreach (var result in cadet.Results.ToList())
            {
                db.Results.DeleteObject(result);
            }
            db.Cadets.DeleteObject(cadet);
            db.SaveChanges();
            return RedirectToAction("Index", new { groupId = gId });
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }


    static class ImageHelper
    {
        public static MemoryStream Shrink(Stream img, int quality = 50, int newWidth = 0, int newHeight = 0)
        {
            if (quality < 10)
                quality = 10;
            else if (quality > 100)
                quality = 100;
            var src = Bitmap.FromStream(img);
            var sizes = calcSizes(src.Width, src.Height, newWidth, newHeight);
            MemoryStream imageStream = new MemoryStream();

            var target = new Bitmap(sizes.X, sizes.Y);
            var g = Graphics.FromImage(target);
            g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBilinear;
            g.DrawImage(src, 0,0, sizes.X,sizes.Y);

            ImageCodecInfo jgpEncoder = getEncoderInfo("image/jpeg");
            var encParams = new EncoderParameters(1);
            encParams.Param[0] = new EncoderParameter(System.Drawing.Imaging.Encoder.Quality, quality);
            target.Save(imageStream, jgpEncoder, encParams);


            return imageStream;
        }

        private static ImageCodecInfo getEncoderInfo(string mimeType)
        {
            // Get image codecs for all image formats
            ImageCodecInfo[] codecs = ImageCodecInfo.GetImageEncoders();

            // Find the correct image codec
            for (int i = 0; i < codecs.Length; i++)
                if (codecs[i].MimeType == mimeType)
                    return codecs[i];
            return null;
        }

        private static Point calcSizes(int srcWidth, int srcHeight, int newWidth, int newHeight)
        {
            if (newWidth < 0)
                newWidth = 0;
            if (newHeight < 0)
                newHeight = 0;

            Point res = new Point(srcWidth, srcHeight);

            
            if (newHeight + newWidth != 0)
            {
                double ratio = (double)srcWidth/srcHeight;
                if (newHeight == 0)
                {
                    res.X = newWidth;
                    res.Y = (int)((double)newWidth / ratio);
                }
                else if (newWidth == 0)
                {
                    res.X = (int)((double)newHeight * ratio);
                    res.Y = newHeight;
                }
                else
                {
                    res.X = newWidth;
                    res.X = newHeight;
                }
            }

            return res;
        }
    }
}