﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Cadet.PhysicalAccounting.DBProxy;

namespace Cadet.PhysicalAccounting.Web.Controllers
{
    public class NormTypeController : Controller
    {
        private DBContext db = new DBContext();

        //
        // GET: /NormType/

        public ActionResult Index()
        {
            if (!User.IsPriveleged())
            {
                return View("AccessDenied");
            }

            return View(db.NormTypes.ToList());
        }

        //
        // GET: /NormType/Details/5

        public ActionResult Details(byte id = 0)
        {
            if (!User.IsPriveleged())
            {
                return View("AccessDenied");
            }

            NormType normtype = db.NormTypes.Single(n => n.Id == id);
            if (normtype == null)
            {
                return HttpNotFound();
            }
            return View(normtype);
        }

        //
        // GET: /NormType/Create

        public ActionResult Create()
        {
            if (!User.IsPriveleged())
            {
                return View("AccessDenied");
            }

            return View();
        }

        //
        // POST: /NormType/Create

        [HttpPost]
        public ActionResult Create(NormType normtype)
        {
            if (!User.IsPriveleged())
            {
                return View("AccessDenied");
            }

            if (ModelState.IsValid)
            {
                db.NormTypes.AddObject(normtype);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(normtype);
        }

        //
        // GET: /NormType/Edit/5

        public ActionResult Edit(byte id = 0)
        {
            if (!User.IsPriveleged())
            {
                return View("AccessDenied");
            }

            NormType normtype = db.NormTypes.Single(n => n.Id == id);
            if (normtype == null)
            {
                return HttpNotFound();
            }
            return View(normtype);
        }

        //
        // POST: /NormType/Edit/5

        [HttpPost]
        public ActionResult Edit(NormType normtype)
        {
            if (!User.IsPriveleged())
            {
                return View("AccessDenied");
            }

            if (ModelState.IsValid)
            {
                db.NormTypes.Attach(normtype);
                db.ObjectStateManager.ChangeObjectState(normtype, EntityState.Modified);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(normtype);
        }

        //
        // GET: /NormType/Delete/5

        public ActionResult Delete(byte id = 0)
        {
            if (!User.IsPriveleged())
            {
                return View("AccessDenied");
            }

            NormType normtype = db.NormTypes.Single(n => n.Id == id);
            if (normtype == null)
            {
                return HttpNotFound();
            }
            return View(normtype);
        }

        //
        // POST: /NormType/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(byte id)
        {
            if (!User.IsPriveleged())
            {
                return View("AccessDenied");
            }

            NormType normtype = db.NormTypes.Single(n => n.Id == id);
            db.NormTypes.DeleteObject(normtype);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}