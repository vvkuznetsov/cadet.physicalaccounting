﻿using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Cadet.PhysicalAccounting.DBProxy;
using Cadet.PhysicalAccounting.Web.Models;
using Cadet.PhysicalAccounting.Web.Models.MockGenerators;

namespace Cadet.PhysicalAccounting.Web.Controllers
{
    public class CadetsDataController : Controller
    {
        public ActionResult Index(int groupId = 0)
        {
            var model = new SessionData();
            model.GroupId = groupId;
            ViewData.Add("currentGroupId", groupId);

            if (groupId > 0)
            {
                using (var ctx = new DBProxy.DBContext())
                {
                    try
                    {
                        model.Cadets = ctx.Cadets.Where(o => o.GroupId == groupId).ToList();
                        model.Group = ctx.Groups.Single(o => o.Id == groupId);
                        model.Course = model.Group.Course;
                        model.Faculty = model.Group.Course.Faculty;
                    }
                    catch (Exception)
                    {
                        model.Info.ErrorMessage =
                            "Произошла ошибка при получении информации из базы данных, возможно искомых записей не существует";
                    }
                }
            }
            else if (groupId < 0)
            {
                // Test data
                model.Info.InfoMessage = "Отображены тестовые данные";

                model.Cadets = CadetsGenerator.GetCadetsList(5);
                model.Group.Number = 5;
                model.Course.Number = 3;
                model.Faculty.Number = 2;
            }
            else
            {
                model.Info.ErrorMessage = "Не указан идентификатор группы";
            }
            return View(model);
        }

        //[HttpPost]
        public ActionResult WriteResults(int groupId)
        {
            if (!User.IsPriveleged())
            {
                return View("AccessDenied");
            }

            using (var context = new DBContext())
            {
                var model = new SessionData {GroupId = groupId, SemesterId = 0};
                ViewData.Add("currentGroupId", groupId);

                FillData(context, model);
                model.CadetResults = model.Cadets.Select(cadet => new CadetResults {Cadet = cadet}).ToList();
                FillResults(context, model);

                return View(model);
            }
        }

        public ActionResult GetResults(int groupId, int semesterId)
        {
            if (!User.IsPriveleged())
            {
                return View("AccessDenied");
            }

            using (var context = new DBContext())
            {
                var model = new SessionData {SemesterId = (byte) semesterId, GroupId = groupId};
                ViewData.Add("groupId", groupId);

                FillData(context, model);
                model.CadetResults = model.Cadets.Select(cadet => new CadetResults {Cadet = cadet}).ToList();
                FillResults(context, model);

                return View("WriteResultsTablePartial", model);
            }
        }

        private static void FillResults(DBContext context, SessionData model)
        {
            try
            {
                foreach (var currentCadet in model.CadetResults)
                {
                    var cadet = currentCadet;
                    var results =
                        context.Results.Where(r => r.SemesterId == model.SemesterId && r.CadetId == cadet.Cadet.Id);

                    Result result;

                    result = results.SingleOrDefault(r => r.Norm.NormType.Id == NormsTypes.PowerId);

                    if (result != null)
                    {
                        model.PowerNormId = result.NormId;
                        cadet.Power = result.Value;
                    }

                    result = results.SingleOrDefault(r => r.Norm.NormType.Id == NormsTypes.AgilityId);

                    if (result != null)
                    {
                        model.AgilityNormId = result.NormId;
                        cadet.Agility = result.Value;
                    }

                    result = results.SingleOrDefault(r => r.Norm.NormType.Id == NormsTypes.SpeedId);

                    if (result != null)
                    {
                        model.SpeedNormId = result.NormId;
                        cadet.Speed = result.Value;
                    }

                    result = results.SingleOrDefault(r => r.Norm.NormType.Id == NormsTypes.EnduranceId);

                    if (result != null)
                    {
                        model.EnduranceNormId = result.NormId;
                        cadet.Endurance = result.Value;
                    }

                    result = results.SingleOrDefault(r => r.Norm.NormType.Id == NormsTypes.MilitaryId);

                    if (result != null)
                    {
                        model.MilitaryNormId = result.NormId;
                        cadet.Military = result.Value;
                    }
                }
            }
            catch (Exception ex)
            {
                model.Info.ErrorMessage = "Произошла ошибка при сохранении информации";
                throw;
            }
        }

        private static void FillData(DBContext context, SessionData model)
        {
            if (model.GroupId > 0)
            {
                try
                {
                    model.Cadets = context.Cadets.Where(o => o.GroupId == model.GroupId).ToList();
                    model.Group = context.Groups.Single(o => o.Id == model.GroupId);
                    model.Course = model.Group.Course;
                    model.Faculty = model.Group.Course.Faculty;

                    model.PowerNorms = new SelectList(
                        context.Norms.Where(norm => norm.NormType.Id == NormsTypes.PowerId).ToList(), "Id",
                        "Name");
                    model.AgilityNorms =
                        new SelectList(context.Norms.Where(norm => norm.NormType.Id == NormsTypes.AgilityId).ToList(),
                                       "Id", "Name");
                    model.SpeedNorms =
                        new SelectList(context.Norms.Where(norm => norm.NormType.Id == NormsTypes.SpeedId).ToList(),
                                       "Id", "Name");
                    model.EnduranceNorms =
                        new SelectList(context.Norms.Where(norm => norm.NormType.Id == NormsTypes.EnduranceId).ToList(), "Id",
                                       "Name");
                    model.MilitaryNorms =
                        new SelectList(context.Norms.Where(norm => norm.NormType.Id == NormsTypes.MilitaryId).ToList(), "Id",
                                       "Name");

                    model.Semesters = new SelectList(context.Semesters.ToList(), "Id", "Number");
                }
                catch (Exception)
                {
                    model.Info.ErrorMessage =
                        "Произошла ошибка при получении информации из базы данных, возможно искомых записей не существует";
                }
            }
            else
            {
                model.Info.ErrorMessage = "Не указан идентификатор группы";
            }
        }

        [HttpPost]
        public ActionResult WriteResults(SessionData model)
        {
            if (!User.IsPriveleged())
            {
                return View("AccessDenied");
            }

            using (var context = new DBContext())
            {
                FillData(context, model);

                for (int i = 0; i < model.CadetResults.Count; i++)
                {
                    model.CadetResults[i].Cadet = model.Cadets[i];
                }

                if (Validate(model))
                {
                    SaveModel(context, model);
                    model.Info.InfoMessage = "Результаты успешно сохранены";

                    return View(model);
                }

                return View(model);
            }
        }

        private void SaveModel(DBContext context, SessionData model)
        {
            try
            {
                foreach (var currentCadet in model.CadetResults)
                {
                    var cadet = currentCadet;
                    var results =
                        context.Results.Where(r => r.SemesterId == model.SemesterId && r.CadetId == cadet.Cadet.Id);

                    Result result;

                    if (model.PowerNormId > 0)
                    {
                        result = results.SingleOrDefault(r => r.Norm.NormType.Id == NormsTypes.PowerId);

                        if (result != null)
                        {
                            result.Value = cadet.Power;
                            result.NormId = model.PowerNormId;
                        }
                        else
                        {
                            context.Results.AddObject(new Result
                                {
                                    Value = cadet.Power,
                                    NormId = model.PowerNormId,
                                    CadetId = cadet.Cadet.Id,
                                    SemesterId = model.SemesterId
                                });
                        }
                    }

                    if (model.AgilityNormId > 0)
                    {
                        result = results.SingleOrDefault(r => r.Norm.NormType.Id == NormsTypes.AgilityId);

                        if (result != null)
                        {
                            result.Value = cadet.Agility;
                            result.NormId = model.AgilityNormId;
                        }
                        else
                        {
                            context.Results.AddObject(new Result
                                {
                                    Value = cadet.Agility,
                                    NormId = model.AgilityNormId,
                                    CadetId = cadet.Cadet.Id,
                                    SemesterId = model.SemesterId
                                });
                        }
                    }

                    if (model.SpeedNormId > 0)
                    {
                        result = results.SingleOrDefault(r => r.Norm.NormType.Id == NormsTypes.SpeedId);

                        if (result != null)
                        {
                            result.Value = cadet.Speed;
                            result.NormId = model.SpeedNormId;
                        }
                        else
                        {
                            context.Results.AddObject(new Result
                                {
                                    Value = cadet.Speed,
                                    NormId = model.SpeedNormId,
                                    CadetId = cadet.Cadet.Id,
                                    SemesterId = model.SemesterId
                                });
                        }
                    }

                    if (model.EnduranceNormId > 0)
                    {
                        result = results.SingleOrDefault(r => r.Norm.NormType.Id == NormsTypes.EnduranceId);

                        if (result != null)
                        {
                            result.Value = cadet.Endurance;
                            result.NormId = model.EnduranceNormId;
                        }
                        else
                        {
                            context.Results.AddObject(new Result
                                {
                                    Value = cadet.Endurance,
                                    NormId = model.EnduranceNormId,
                                    CadetId = cadet.Cadet.Id,
                                    SemesterId = model.SemesterId
                                });
                        }
                    }

                    if (model.MilitaryNormId > 0)
                    {
                        result = results.SingleOrDefault(r => r.Norm.NormType.Id == NormsTypes.MilitaryId);

                        if (result != null)
                        {
                            result.Value = cadet.Military;
                            result.NormId = model.MilitaryNormId;
                        }
                        else
                        {
                            context.Results.AddObject(new Result
                                {
                                    Value = cadet.Military,
                                    NormId = model.MilitaryNormId,
                                    CadetId = cadet.Cadet.Id,
                                    SemesterId = model.SemesterId
                                });
                        }
                    }

                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                model.Info.ErrorMessage = "Произошла ошибка при сохранении информации";
                throw;
            }
        }

        private bool Validate(SessionData model)
        {
            var flags = new[] { true, true, true, true, true };

            if (model.PowerNormId == 0 &&
                model.AgilityNormId == 0 &&
                model.SpeedNormId == 0 &&
                model.EnduranceNormId == 0 &&
                model.MilitaryNormId == 0)
            {
                model.Info.ErrorMessage = "Выберете номер упражнения для сохранения результатов.";
                return false;
            }

            model.CadetResults.Aggregate(flags, (seed, cadet) =>
                {
                    seed[0] = seed[0] && (cadet.Power >= 0);
                    seed[1] = seed[1] && (cadet.Agility >= 0);
                    seed[2] = seed[2] && (cadet.Speed >= 0);
                    seed[3] = seed[3] && (cadet.Endurance >= 0);
                    seed[4] = seed[4] && (cadet.Military >= 0);

                    return seed;
                });

            if (model.PowerNormId > 0 && !flags[0])
            {
                model.Info.ErrorMessage = string.Format("Перед сохранением введите все результаты в категории \"{0}\".", NormsTypes.Power);

                return false;
            }

            if (model.AgilityNormId > 0 && !flags[1])
            {
                model.Info.ErrorMessage = string.Format("Перед сохранением введите все результаты в категории \"{0}\".", NormsTypes.Agility);

                return false;
            }

            if (model.SpeedNormId > 0 && !flags[2])
            {
                model.Info.ErrorMessage = string.Format("Перед сохранением введите все результаты в категории \"{0}\".", NormsTypes.Speed);

                return false;
            }

            if (model.EnduranceNormId > 0 && !flags[3])
            {
                model.Info.ErrorMessage = string.Format("Перед сохранением введите все результаты в категории \"{0}\".", NormsTypes.Endurance);

                return false;
            }

            if (model.MilitaryNormId > 0 && !flags[4])
            {
                model.Info.ErrorMessage = string.Format("Перед сохранением введите все результаты в категории \"{0}\".", NormsTypes.Military);

                return false;
            }

            return true;
        }

        //[HttpPost]
        public ActionResult Details(int cadetId)
        {
            var model = new DBProxy.Cadet();
            
            using (var ctx = new DBProxy.DBContext())
            {
                if (cadetId > 0)
                {
                    model = ctx.Cadets.Include("Rank")
                        .Include("Group")
                        .Include("Group.Course")
                        .Include("Group.Course.Faculty")
                        .Include("Results")
                        .Include("Results.Norm")
                        .Include("Results.Norm.NormType")
                        .Include("Results.Semester")
                        .SingleOrDefault(o => o.Id == cadetId);


                    ViewData.Add("currentGroupId", model.GroupId);
                    if (model == null)
                    {
                        model.Info.ErrorMessage = "Курсант не найден";
                    }
                }
                else if (cadetId < 0)
                {
                    // Test data
                    model.Info.InfoMessage = "Отображены тестовые данные";

                    model = CadetsGenerator.GetCadet();
                }
                else
                {
                    model.Info.ErrorMessage = "Не указан идентификатор курсанта";
                }

                return View(model);
            }
        }


        public ActionResult Report(int cadetId)
        {
            var model = new DBProxy.Cadet();

            using (var ctx = new DBProxy.DBContext())
            {
                //ctx.Results.First().Norm.no
                model = ctx.Cadets.Include("Rank")
                    .Include("Group")
                    .Include("Group.Course")
                    .Include("Group.Course.Faculty")
                    .Include("Results.Semester")
                    .Include("Results.Norm")
                    .Include("Results.Norm.NormType")
                    .SingleOrDefault(o => o.Id == cadetId);
                //var t = model.Group.CompiledGroupNumber;

            }

            return View(model);
        }

        public ActionResult Search(string searchString)
        {       
            var model = new SearchSessionData();

            if (!String.IsNullOrEmpty(searchString))
            {
                using (var ctx = new DBProxy.DBContext())
                {
                    try
                    {
                        var search = ctx.Search(searchString).ToList();

                        model.Cadets = search.Join(
                                ctx.Groups,
                                cadet => cadet.GroupId,
                                group => group.Id,
                                (cadet, group) =>
                                    {
                                        cadet.Group = group;
                                        return cadet;
                                    }
                                ).ToList();

                        model.Cadets.ForEach(c => c.Group.CalcCompiledGroupNumber());
                        model.SearchString = searchString;
                    }
                    catch (Exception)
                    {
                        model.Info.ErrorMessage =
                            "Произошла ошибка при получении информации из базы данных, возможно искомых записей не существует";
                    }
                }
            }
            else
            {
                // Test data
                model.Info.InfoMessage = "Введен пустой поисковый запрос";
            }
            return View(model);
        }
    }


}
