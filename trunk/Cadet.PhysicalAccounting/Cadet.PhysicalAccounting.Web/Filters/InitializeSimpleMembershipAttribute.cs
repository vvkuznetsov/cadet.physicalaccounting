﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Objects;
using System.Data.Objects.DataClasses;
using System.Threading;
using System.Web.Mvc;
using System.Web.Security;
using Cadet.PhysicalAccounting.DBProxy;
using Cadet.PhysicalAccounting.Web.Models;
using WebMatrix.WebData;
using System.Linq;

namespace Cadet.PhysicalAccounting.Web.Filters
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = false, Inherited = true)]
    public sealed class InitializeSimpleMembershipAttribute : ActionFilterAttribute
    {
        private static SimpleMembershipInitializer _initializer;
        private static object _initializerLock = new object();
        private static bool _isInitialized;

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            // Ensure ASP.NET Simple Membership is initialized only once per app start
            LazyInitializer.EnsureInitialized(ref _initializer, ref _isInitialized, ref _initializerLock);
        }

        private class SimpleMembershipInitializer
        {

            public SimpleMembershipInitializer()
            {
                Database.SetInitializer<UsersContext>(null);

                try
                {
                    using (var context = new UsersContext())
                    {
                        if (!context.Database.Exists())
                        {
                            // Create the SimpleMembership database without Entity Framework migration schema
                            ((IObjectContextAdapter)context).ObjectContext.CreateDatabase();
                        }
                    }

                    WebSecurity.InitializeDatabaseConnection("DefaultConnection", "UserProfile", "UserId", "UserName", autoCreateTables: true);

                    var roles = (SimpleRoleProvider)Roles.Provider;
                    var membership = (SimpleMembershipProvider)Membership.Provider;

                    CreateUser(roles, membership, "admin", Extensions.Roles.Admin);
                    CreateUser(roles, membership, "secretary", Extensions.Roles.Secretary);
                    CreateUser(roles, membership, "user", Extensions.Roles.User);
                }
                catch (Exception ex)
                {
                    throw new InvalidOperationException("The ASP.NET Simple Membership database could not be initialized. For more information, please see http://go.microsoft.com/fwlink/?LinkId=256588", ex);
                }
            }

            private static void CreateUser(SimpleRoleProvider roles, SimpleMembershipProvider membership, string userName, string roleName)
            {
                if (!roles.RoleExists(roleName))
                {
                    roles.CreateRole(roleName);
                } 

                if (membership.GetUser(userName, false) == null)
                {
                    membership.CreateUserAndAccount(userName, userName);
                }

                if (!roles.GetRolesForUser(userName).Contains(roleName))
                {
                    roles.AddUsersToRoles(new[] { userName }, new[] { roleName });
                }
            }
        }
    }
}
