﻿namespace Cadet.PhysicalAccounting.Web.Extensions
{
    static class Roles
    {
        public static string Admin { get { return "Admin"; } }
        public static string Secretary { get { return "Secretary"; } }
        public static string User { get { return "User"; } }
    }
}