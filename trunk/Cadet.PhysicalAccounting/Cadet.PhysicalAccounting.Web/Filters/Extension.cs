﻿using System.Security.Principal;
using Cadet.PhysicalAccounting.Web.Extensions;

namespace Cadet.PhysicalAccounting.Web

{
    public static class Extension
    {
        public static bool IsAdmin(this IPrincipal user)
        {
            return user.IsInRole(Roles.Admin);
        }

        public static bool IsSecretary(this IPrincipal user)
        {
            return user.IsInRole(Roles.Secretary);
        }

        public static bool IsUser(this IPrincipal user)
        {
            return user.IsInRole(Roles.User);
        }

        public static bool IsDebugOn(this IPrincipal user)
        {
            return AuthConfig.DebugMode;
        }

        /// <summary>
        /// Возвращает truе, если это админ или секретарь
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public static bool IsPriveleged(this IPrincipal user)
        {
            return user.IsDebugOn() || user.IsSecretary() || user.IsAdmin();
        }
    }
}