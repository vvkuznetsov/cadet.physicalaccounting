﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Cadet.PhysicalAccounting.Web.Models
{
    public class FacultyTreeModel
    {
        public FacultyTreeModel()
        {
            Root = new TreeItem(0, "Root");
        }

        public TreeItem Root { get; set; }
    }

    public class TreeItem
    {
        public TreeItem(int id, string name)
        {
            Id = id;
            Name = name;
            Childs = new List<TreeItem>();
        }

        public TreeItem AddChild(int Id, string Name)
        {
            Childs.Add(new TreeItem(Id,Name));
            return Childs.Last();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public List<TreeItem> Childs { get; set; } 
    }
}