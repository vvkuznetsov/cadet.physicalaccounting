﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Cadet.PhysicalAccounting.Web.Models.MockGenerators
{
    public static class CadetsGenerator
    {
        public static DBProxy.Cadet GetCadet(string surname = "Иванов", 
                                             string name = "Иван", 
                                             string fathersname = "Иванович")
        {
            var result = new DBProxy.Cadet();

            result.FirstName = name;
            result.LastName = surname;
            result.FathersName = fathersname;

            return result;
        }


        public static List<DBProxy.Cadet> GetCadetsList(int amount)
        {
            var result = new List<DBProxy.Cadet>();

            var fios = new List<List<string>>
            {
                new List<string> {"Иванов", "Иван", "Иванович"},
                new List<string> {"Петров", "Пётр", "Петрович"},
                new List<string> {"Семёнов", "Семён", "Семёнович"},
            };
                
            int fio_index = 0;
            for (int i = 0; i < amount; i++)
            {
                if (fio_index == fios.Count)
                    fio_index = 0;

                var cadet = new DBProxy.Cadet();
                cadet.LastName = fios[fio_index][0];
                cadet.FirstName = fios[fio_index][1];
                cadet.FathersName = fios[fio_index][2];
                result.Add(cadet);

                fio_index++;
            }

            return result;
        }
    }
}