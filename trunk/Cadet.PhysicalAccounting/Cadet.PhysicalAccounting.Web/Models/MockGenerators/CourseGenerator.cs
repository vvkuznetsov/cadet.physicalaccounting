﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Cadet.PhysicalAccounting.Web.Models.MockGenerators
{
    public static class CourseGenerator
    {
        public static DBProxy.Course GetCourse(int id, int number)
        {
            var result = new DBProxy.Course();

            result.Id = id;
            result.Number = number;
            result.Groups = new System.Data.Objects.DataClasses.EntityCollection<DBProxy.Group>();

            var groups = GroupGenerator.GetGroupList();
            groups.ForEach(x => result.Groups.Add(x));

            return result;
        }


        public static List<DBProxy.Course> GetCourseList(int amount = 5)
        {
            var result = new List<DBProxy.Course>();
                
            for (int i = 0; i < amount; i++)
            {
                result.Add(GetCourse(i, i + 1));
            }

            return result;
        }


    }
}