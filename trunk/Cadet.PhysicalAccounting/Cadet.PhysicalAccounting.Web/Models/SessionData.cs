﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using System.Web.Mvc;
using Cadet.PhysicalAccounting.DBProxy;
using Cadet.PhysicalAccounting.DBProxy.Extensions;

namespace Cadet.PhysicalAccounting.Web.Models
{
    /// <summary>
    /// Данные для просмотре информации о курсанте и проставления результатов
    /// </summary>
    [Serializable()]
    public class SessionData
    {
        public SessionData()
        {
            Info = new BaseRazorModel();
            Cadets = new List<DBProxy.Cadet>();
            Faculty = new Faculty();
            Course = new Course();
            Group = new Group();
        }

        public BaseRazorModel Info { get; set; }

        public List<DBProxy.Cadet> Cadets { get; set; }
        public Faculty Faculty { get; set; }
        public Course Course { get; set; }

        // Номер группы и группа
        public int GroupId { get; set; }
        public Group Group { get; set; }

        // Поля для выбора номера упражнения при вводе нормативов
        public int PowerNormId { get; set; }
        public int AgilityNormId { get; set; }
        public int SpeedNormId { get; set; }
        public int EnduranceNormId { get; set; }
        public int MilitaryNormId { get; set; }

        // Списки для наполнения дропдауна для выбора номера упражнения при вводе нормативов
        public SelectList PowerNorms { get; set; }
        public SelectList AgilityNorms { get; set; }
        public SelectList SpeedNorms { get; set; }
        public SelectList EnduranceNorms { get; set; }
        public SelectList MilitaryNorms { get; set; }

        // Спсисок кадетов для ввода результатов по нормативам
        //[DataMemberAttribute()]
        public List<CadetResults> CadetResults { get; set; }

        public Byte SemesterId { get; set; }
        public SelectList Semesters { get; set; }
    }

    //[Serializable()]
    public class CadetResults
    {
        public BaseRazorModel Info = new BaseRazorModel();

        public DBProxy.Cadet Cadet;

        //[DataMemberAttribute()]
        public int Power { get; set; }

        //[DataMemberAttribute()]
        public int Agility { get; set; }

        //[DataMemberAttribute()]
        public int Speed { get; set; }

        //[DataMemberAttribute()]
        public int Endurance { get; set; }

        //[DataMemberAttribute()]
        public int Military { get; set; }
    }
    
}