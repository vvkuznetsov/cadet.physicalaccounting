﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Cadet.PhysicalAccounting.Web.Models
{
    public class SearchSessionData : SessionData
    {
        public string SearchString { get; set; }
    }
}