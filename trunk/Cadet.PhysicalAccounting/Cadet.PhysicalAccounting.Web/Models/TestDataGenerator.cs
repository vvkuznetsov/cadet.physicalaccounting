﻿using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Data.Objects.DataClasses;
using System.Linq;
using Cadet.PhysicalAccounting.DBProxy;

namespace Cadet.PhysicalAccounting.Web.Models
{
    internal class TestDataGenerator
    {
        public void FillDB()
        {
            using (var db = new DBProxy.DBContext())
            {
                // звания
                FillRanks(db);

                // Факультеты
                FillFaculties(db);

                // Семестры

                FillSemesters(db);
                db.SaveChanges();

                // Курсы
                FillCourses(db);
                db.SaveChanges();

                // Группы
                FillGroups(db);
                db.SaveChanges();

                // Кадеты
                FillCadets(db);
                db.SaveChanges();

                // Виды нормативов, результаты и всякое такое
                FillNorms(db);
                db.SaveChanges();
            }
        }

        private static void FillNorms(DBContext db)
        {
            var normTypes = new List<string>
                {
                    "Сила",
                    "Ловкость",
                    "Быстрота",
                    "Выносливость",
                    "ВПН"
                };

            //список границ для результатов
            var results = new Dictionary<string, EntityCollection<Result>>();
            var random = new Random(DateTime.Now.Millisecond);

            foreach (var normType in normTypes)
            {
                var newResult = new EntityCollection<Result>();

                for (byte i = 1; i <= 10; i++)
                {
                    foreach (var cadet in db.Cadets)
                    {
                        var result = new Result {Value = random.Next(90 + i*5), SemesterId = i, CadetId = cadet.Id};
                        newResult.Add(result);
                        //db.Results.AddObject(result);
                    }
                }
                results.Add(normType, newResult);
            }

            for (int i = 0; i < normTypes.Count; i++)
            {
                var normType = normTypes[i];
                var norm = "N" + i;

                if (!db.NormTypes.Any(entity => entity.Name == normType))
                {
                    var newNormType = new NormType
                        {
                            Name = normType,
                        };


                    var newNorm = new Norm
                        {
                            Name = norm,
                            Description = norm + " Description",
                            NormType = newNormType,
                            Results = results[normType]
                        };

                    db.NormTypes.AddObject(newNormType);
                }
            }
        }

        private static void FillCadets(DBContext db)
        {
            var cadets = new List<List<string>>
                {
                    new List<string> {"Иванов", "Иван", "Иванович"},
                    new List<string> {"Петров", "Пётр", "Петрович"},
                    new List<string> {"Семёнов", "Семён", "Семёнович"},
                };


            foreach (var course in db.Faculties.Take(1).Single().Courses)
            {
                foreach (var group in course.Groups)
                {
                    foreach (var cadet in cadets)
                    {
                        if (@group.Cadets.All(entity => entity.LastName != cadet[0]))
                        {
                            db.Cadets.AddObject(new DBProxy.Cadet
                                {
                                    LastName = cadet[0],
                                    FirstName = cadet[1],
                                    FathersName = cadet[2],
                                    Group = @group,
                                    Rank = db.Ranks.ToArray()[0]
                                });
                        }
                    }
                }
            }
        }

        private static void FillGroups(DBContext db)
        {
            var groups = new List<int> {1, 2, 3, 4, 5, 6, 7, 8};

            foreach (var course in db.Courses)
            {
                foreach (var group in groups)
                {
                    if (course.Groups.All(entity => entity.Number != @group))
                    {
                        db.Groups.AddObject(new Group
                            {
                                Number = @group,
                                Course = course,
                                Speciality = "123",
                                SemesterId = (byte) (course.Number*2 - 1)
                            });
                    }
                }
            }
        }

        private static void FillCourses(DBContext db)
        {
            var courses = new List<int> {1, 2, 3, 4, 5};

            foreach (var faculty in db.Faculties)
            {
                foreach (var course in courses)
                {
                    if (faculty.Courses.All(entity => entity.Number != course))
                    {
                        db.Courses.AddObject(new Course
                            {
                                Number = course,
                                Faculty = faculty
                            });
                    }
                }
            }
        }

        private static void FillSemesters(DBContext db)
        {
            var semesters = new List<int> {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
            foreach (var semester in semesters)
            {
                if (!db.Semesters.Any(entity => entity.Number == semester))
                {
                    db.Semesters.AddObject(new Semester { Number = semester, Threshold = 90 + semester * 5 });
                }
            }
        }

        private static void FillFaculties(DBContext db)
        {
            var facs = new List<string>
                {
                    "Конструкции летательных аппаратов",
                    "Систем управления ракетно-космических комплексов",
                    "Радиоэлектронных систем космических комплексов",
                    "Наземной космической инфраструктуры",
                    "Сбора и обработки информации",
                    "Информационного обеспечения и вычислительной техники",
                    "Топогеодезического обеспечения и картографии",
                    "Средств ракетно-космической обороны",
                    "Автоматизированных систем управления войсками"
                };


            for (int i = 0; i < facs.Count; i++)
            {
                var fac = facs[i];

                if (!db.Faculties.Any(entity => entity.Title == fac))
                {
                    db.Faculties.AddObject(new Faculty() {Number = i + 1, Title = fac});
                }
            }
        }

        private static void FillRanks(DBContext db)
        {
// Звания
            var ranks = new List<string>
                {
                    "рядовой",
                    "курсант",
                    "ефрейтор",
                    "младший сержант",
                    "сержант",
                    "старший сержант",
                    "младший лейтенант",
                    "лейтенант",
                    "старший лейтенант",
                    "капитан"
                };

            foreach (var rank in ranks)
            {
                if (!db.Ranks.Any(entity => entity.Name == rank))
                {
                    db.Ranks.AddObject(new Rank {Name = rank});
                }
            }
        }
    }
}