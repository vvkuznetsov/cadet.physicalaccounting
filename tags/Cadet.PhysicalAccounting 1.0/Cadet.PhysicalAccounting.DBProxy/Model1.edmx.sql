
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, and Azure
-- --------------------------------------------------
-- Date Created: 07/24/2013 11:14:00
-- Generated from EDMX file: F:\project\military\Cadet.PhysicalAccounting\trunk\Cadet.PhysicalAccounting\Cadet.PhysicalAccounting.DBProxy\Model1.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [Cadet.PhysicalAccounting];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[FK_CourseGroup]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Groups] DROP CONSTRAINT [FK_CourseGroup];
GO
IF OBJECT_ID(N'[dbo].[FK_FacultyCourse]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Courses] DROP CONSTRAINT [FK_FacultyCourse];
GO
IF OBJECT_ID(N'[dbo].[FK_GroupCadet]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Cadets] DROP CONSTRAINT [FK_GroupCadet];
GO
IF OBJECT_ID(N'[dbo].[FK_NormResult]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Results] DROP CONSTRAINT [FK_NormResult];
GO
IF OBJECT_ID(N'[dbo].[FK_NormTypeNorm]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Norms] DROP CONSTRAINT [FK_NormTypeNorm];
GO
IF OBJECT_ID(N'[dbo].[FK_RankCadet]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Cadets] DROP CONSTRAINT [FK_RankCadet];
GO
IF OBJECT_ID(N'[dbo].[FK_ResultCadet]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Results] DROP CONSTRAINT [FK_ResultCadet];
GO
IF OBJECT_ID(N'[dbo].[FK_ResultSemester]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Results] DROP CONSTRAINT [FK_ResultSemester];
GO
IF OBJECT_ID(N'[dbo].[FK_SemesterGroup]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Groups] DROP CONSTRAINT [FK_SemesterGroup];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[Cadets]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Cadets];
GO
IF OBJECT_ID(N'[dbo].[Courses]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Courses];
GO
IF OBJECT_ID(N'[dbo].[Faculties]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Faculties];
GO
IF OBJECT_ID(N'[dbo].[Groups]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Groups];
GO
IF OBJECT_ID(N'[dbo].[Norms]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Norms];
GO
IF OBJECT_ID(N'[dbo].[NormTypes]', 'U') IS NOT NULL
    DROP TABLE [dbo].[NormTypes];
GO
IF OBJECT_ID(N'[dbo].[Ranks]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Ranks];
GO
IF OBJECT_ID(N'[dbo].[Results]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Results];
GO
IF OBJECT_ID(N'[dbo].[Semesters]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Semesters];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'Cadets'
CREATE TABLE [dbo].[Cadets] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [FirstName] nvarchar(max)  NOT NULL,
    [LastName] nvarchar(max)  NULL,
    [FathersName] nvarchar(max)  NULL,
    [Photo] varbinary(max)  NULL,
    [Year] int  NOT NULL,
    [GroupId] int  NOT NULL,
    [RankId] tinyint  NOT NULL
);
GO

-- Creating table 'Ranks'
CREATE TABLE [dbo].[Ranks] (
    [Id] tinyint IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'Groups'
CREATE TABLE [dbo].[Groups] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Number] int  NOT NULL,
    [CourseId] int  NOT NULL,
    [Speciality] nvarchar(max)  NOT NULL,
    [SemesterId] tinyint  NOT NULL
);
GO

-- Creating table 'Faculties'
CREATE TABLE [dbo].[Faculties] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Number] int  NOT NULL,
    [Title] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'Courses'
CREATE TABLE [dbo].[Courses] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [FacultyId] int  NOT NULL,
    [Number] int  NOT NULL
);
GO

-- Creating table 'NormTypes'
CREATE TABLE [dbo].[NormTypes] (
    [Id] tinyint IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'Norms'
CREATE TABLE [dbo].[Norms] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [Description] nvarchar(max)  NOT NULL,
    [NormTypeId] tinyint  NOT NULL
);
GO

-- Creating table 'Semesters'
CREATE TABLE [dbo].[Semesters] (
    [Id] tinyint IDENTITY(1,1) NOT NULL,
    [Number] int  NOT NULL,
    [Threshold] int  NOT NULL
);
GO

-- Creating table 'Results'
CREATE TABLE [dbo].[Results] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [CadetId] int  NOT NULL,
    [Value] int  NOT NULL,
    [SemesterId] tinyint  NOT NULL,
    [NormId] int  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [Id] in table 'Cadets'
ALTER TABLE [dbo].[Cadets]
ADD CONSTRAINT [PK_Cadets]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Ranks'
ALTER TABLE [dbo].[Ranks]
ADD CONSTRAINT [PK_Ranks]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Groups'
ALTER TABLE [dbo].[Groups]
ADD CONSTRAINT [PK_Groups]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Faculties'
ALTER TABLE [dbo].[Faculties]
ADD CONSTRAINT [PK_Faculties]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Courses'
ALTER TABLE [dbo].[Courses]
ADD CONSTRAINT [PK_Courses]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'NormTypes'
ALTER TABLE [dbo].[NormTypes]
ADD CONSTRAINT [PK_NormTypes]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Norms'
ALTER TABLE [dbo].[Norms]
ADD CONSTRAINT [PK_Norms]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Semesters'
ALTER TABLE [dbo].[Semesters]
ADD CONSTRAINT [PK_Semesters]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Results'
ALTER TABLE [dbo].[Results]
ADD CONSTRAINT [PK_Results]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [GroupId] in table 'Cadets'
ALTER TABLE [dbo].[Cadets]
ADD CONSTRAINT [FK_GroupCadet]
    FOREIGN KEY ([GroupId])
    REFERENCES [dbo].[Groups]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_GroupCadet'
CREATE INDEX [IX_FK_GroupCadet]
ON [dbo].[Cadets]
    ([GroupId]);
GO

-- Creating foreign key on [FacultyId] in table 'Courses'
ALTER TABLE [dbo].[Courses]
ADD CONSTRAINT [FK_FacultyCourse]
    FOREIGN KEY ([FacultyId])
    REFERENCES [dbo].[Faculties]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_FacultyCourse'
CREATE INDEX [IX_FK_FacultyCourse]
ON [dbo].[Courses]
    ([FacultyId]);
GO

-- Creating foreign key on [CourseId] in table 'Groups'
ALTER TABLE [dbo].[Groups]
ADD CONSTRAINT [FK_CourseGroup]
    FOREIGN KEY ([CourseId])
    REFERENCES [dbo].[Courses]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_CourseGroup'
CREATE INDEX [IX_FK_CourseGroup]
ON [dbo].[Groups]
    ([CourseId]);
GO

-- Creating foreign key on [CadetId] in table 'Results'
ALTER TABLE [dbo].[Results]
ADD CONSTRAINT [FK_ResultCadet]
    FOREIGN KEY ([CadetId])
    REFERENCES [dbo].[Cadets]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_ResultCadet'
CREATE INDEX [IX_FK_ResultCadet]
ON [dbo].[Results]
    ([CadetId]);
GO

-- Creating foreign key on [SemesterId] in table 'Results'
ALTER TABLE [dbo].[Results]
ADD CONSTRAINT [FK_ResultSemester]
    FOREIGN KEY ([SemesterId])
    REFERENCES [dbo].[Semesters]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_ResultSemester'
CREATE INDEX [IX_FK_ResultSemester]
ON [dbo].[Results]
    ([SemesterId]);
GO

-- Creating foreign key on [NormTypeId] in table 'Norms'
ALTER TABLE [dbo].[Norms]
ADD CONSTRAINT [FK_NormTypeNorm]
    FOREIGN KEY ([NormTypeId])
    REFERENCES [dbo].[NormTypes]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_NormTypeNorm'
CREATE INDEX [IX_FK_NormTypeNorm]
ON [dbo].[Norms]
    ([NormTypeId]);
GO

-- Creating foreign key on [RankId] in table 'Cadets'
ALTER TABLE [dbo].[Cadets]
ADD CONSTRAINT [FK_RankCadet]
    FOREIGN KEY ([RankId])
    REFERENCES [dbo].[Ranks]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_RankCadet'
CREATE INDEX [IX_FK_RankCadet]
ON [dbo].[Cadets]
    ([RankId]);
GO

-- Creating foreign key on [NormId] in table 'Results'
ALTER TABLE [dbo].[Results]
ADD CONSTRAINT [FK_NormResult]
    FOREIGN KEY ([NormId])
    REFERENCES [dbo].[Norms]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_NormResult'
CREATE INDEX [IX_FK_NormResult]
ON [dbo].[Results]
    ([NormId]);
GO

-- Creating foreign key on [SemesterId] in table 'Groups'
ALTER TABLE [dbo].[Groups]
ADD CONSTRAINT [FK_SemesterGroup]
    FOREIGN KEY ([SemesterId])
    REFERENCES [dbo].[Semesters]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_SemesterGroup'
CREATE INDEX [IX_FK_SemesterGroup]
ON [dbo].[Groups]
    ([SemesterId]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------