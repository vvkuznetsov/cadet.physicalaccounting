﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Cadet.PhysicalAccounting.DBProxy;

namespace Cadet.PhysicalAccounting.Web.Controllers
{
    public class SemesterController : Controller
    {
        private DBContext db = new DBContext();

        //
        // GET: /Semester/

        public ActionResult Index()
        {
            return View(db.Semesters.ToList());
        }

        //
        // GET: /Semester/Details/5

        public ActionResult Details(byte id = 0)
        {
            Semester semester = db.Semesters.Single(s => s.Id == id);
            if (semester == null)
            {
                return HttpNotFound();
            }
            return View(semester);
        }

        //
        // GET: /Semester/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Semester/Create

        [HttpPost]
        public ActionResult Create(Semester semester)
        {
            if (ModelState.IsValid)
            {
                db.Semesters.AddObject(semester);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(semester);
        }

        //
        // GET: /Semester/Edit/5

        public ActionResult Edit(byte id = 0)
        {
            Semester semester = db.Semesters.Single(s => s.Id == id);
            if (semester == null)
            {
                return HttpNotFound();
            }
            return View(semester);
        }

        //
        // POST: /Semester/Edit/5

        [HttpPost]
        public ActionResult Edit(Semester semester)
        {
            if (ModelState.IsValid)
            {
                db.Semesters.Attach(semester);
                db.ObjectStateManager.ChangeObjectState(semester, EntityState.Modified);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(semester);
        }

        //
        // GET: /Semester/Delete/5

        public ActionResult Delete(byte id = 0)
        {
            Semester semester = db.Semesters.Single(s => s.Id == id);
            if (semester == null)
            {
                return HttpNotFound();
            }
            return View(semester);
        }

        //
        // POST: /Semester/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(byte id)
        {
            Semester semester = db.Semesters.Single(s => s.Id == id);
            db.Semesters.DeleteObject(semester);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}