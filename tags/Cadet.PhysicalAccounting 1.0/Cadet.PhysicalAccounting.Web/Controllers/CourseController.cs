﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Cadet.PhysicalAccounting.DBProxy;

namespace Cadet.PhysicalAccounting.Web.Controllers
{
    public class CourseController : Controller
    {
        private DBContext db = new DBContext();

        //
        // GET: /Course/

        public ActionResult Index(int facultyId = -1)
        {
            if (!User.IsPriveleged())
            {
                return View("AccessDenied");
            }

            var courses = db.Courses.Where(c => c.FacultyId == facultyId).Include("Faculty");
            var fac = db.Faculties.First(f=>f.Id == facultyId);
            ViewBag.FacultyName = fac != null ? fac.Title : "Несуществующий факультет";
            ViewBag.FacultyId = facultyId;
            return View(courses.ToList().OrderBy(c=>c.Number));
        }

        //
        // GET: /Course/Details/5

        public ActionResult Details(int id = 0)
        {
            if (!User.IsPriveleged())
            {
                return View("AccessDenied");
            }

            Course course = db.Courses.Single(c => c.Id == id);
            if (course == null)
            {
                return HttpNotFound();
            }
            return View(course);
        }

        //
        // GET: /Course/Create

        public ActionResult Create(int facultyId = -1)
        {
            if (!User.IsPriveleged())
            {
                return View("AccessDenied");
            }

            ViewBag.FacultyId = new SelectList(db.Faculties, "Id", "Title");
            
            ViewBag.SemesterId = new SelectList(db.Semesters, "Id", "Id");
            ViewBag.SelectedFacultyId = facultyId;
            return View();
        }

        //
        // POST: /Course/Create

        [HttpPost]
        public ActionResult Create(Course course)
        {
            if (!User.IsPriveleged())
            {
                return View("AccessDenied");
            }

            if (ModelState.IsValid)
            {
                db.Courses.AddObject(course);
                db.SaveChanges();
                FacultyTreeMaker.RefreshFacultyTreeModel();
                return RedirectToAction("Index", new  {facultyId = course.FacultyId });
            }

            ViewBag.FacultyId = new SelectList(db.Faculties, "Id", "Title", course.FacultyId);
            return View(course);
        }

        //
        // GET: /Course/Edit/5

        public ActionResult Edit(int id = 0)
        {
            if (!User.IsPriveleged())
            {
                return View("AccessDenied");
            }

            Course course = db.Courses.Single(c => c.Id == id);
            if (course == null)
            {
                return HttpNotFound();
            }
            ViewBag.FacultyId = new SelectList(db.Faculties, "Id", "Title", course.FacultyId);
            return View(course);
        }

        //
        // POST: /Course/Edit/5

        [HttpPost]
        public ActionResult Edit(Course course)
        {
            if (!User.IsPriveleged())
            {
                return View("AccessDenied");
            }

            if (ModelState.IsValid)
            {
                db.Courses.Attach(course);
                db.ObjectStateManager.ChangeObjectState(course, EntityState.Modified);
                db.SaveChanges();
                FacultyTreeMaker.RefreshFacultyTreeModel();
                return RedirectToAction("Index", new { facultyId = course.FacultyId });
            }
            ViewBag.FacultyId = new SelectList(db.Faculties, "Id", "Title", course.FacultyId);
            return View(course);
        }

        //
        // GET: /Course/Delete/5

        public ActionResult Delete(int id = 0)
        {
            if (!User.IsPriveleged())
            {
                return View("AccessDenied");
            }

            Course course = db.Courses.Single(c => c.Id == id);
            if (course == null)
            {
                return HttpNotFound();
            }
            return View(course);
        }

        //
        // POST: /Course/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            if (!User.IsPriveleged())
            {
                return View("AccessDenied");
            }

            Course course = db.Courses.Single(c => c.Id == id);
            int facId = course.FacultyId;
            foreach (var group in course.Groups.ToList())
            {
                foreach (var cadet in group.Cadets.ToList())
                {
                    foreach (var result in cadet.Results.ToList())
                    {
                        db.Results.DeleteObject(result);
                    }
                    db.Cadets.DeleteObject(cadet);
                }
                db.Groups.DeleteObject(group);
            }
            db.Courses.DeleteObject(course);
            db.SaveChanges();
            FacultyTreeMaker.RefreshFacultyTreeModel();
            return RedirectToAction("Index", new { facultyId = facId });
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}