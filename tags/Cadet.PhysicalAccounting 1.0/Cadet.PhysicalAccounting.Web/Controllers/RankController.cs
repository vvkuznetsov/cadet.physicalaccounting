﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Cadet.PhysicalAccounting.DBProxy;

namespace Cadet.PhysicalAccounting.Web.Controllers
{
    public class RankController : Controller
    {
        private DBContext db = new DBContext();

        //
        // GET: /Rank/

        public ActionResult Index()
        {
            if (!User.IsPriveleged())
            {
                return View("AccessDenied");
            }

            return View(db.Ranks.ToList());
        }

        //
        // GET: /Rank/Details/5

        public ActionResult Details(byte id = 0)
        {
            if (!User.IsPriveleged())
            {
                return View("AccessDenied");
            }

            Rank rank = db.Ranks.Single(r => r.Id == id);
            if (rank == null)
            {
                return HttpNotFound();
            }
            return View(rank);
        }

        //
        // GET: /Rank/Create

        public ActionResult Create()
        {
            if (!User.IsPriveleged())
            {
                return View("AccessDenied");
            }

            return View();
        }

        //
        // POST: /Rank/Create

        [HttpPost]
        public ActionResult Create(Rank rank)
        {
            if (!User.IsPriveleged())
            {
                return View("AccessDenied");
            }

            if (ModelState.IsValid)
            {
                db.Ranks.AddObject(rank);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(rank);
        }

        //
        // GET: /Rank/Edit/5

        public ActionResult Edit(byte id = 0)
        {
            if (!User.IsPriveleged())
            {
                return View("AccessDenied");
            }

            Rank rank = db.Ranks.Single(r => r.Id == id);
            if (rank == null)
            {
                return HttpNotFound();
            }
            return View(rank);
        }

        //
        // POST: /Rank/Edit/5

        [HttpPost]
        public ActionResult Edit(Rank rank)
        {
            if (!User.IsPriveleged())
            {
                return View("AccessDenied");
            }

            if (ModelState.IsValid)
            {
                db.Ranks.Attach(rank);
                db.ObjectStateManager.ChangeObjectState(rank, EntityState.Modified);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(rank);
        }

        //
        // GET: /Rank/Delete/5

        public ActionResult Delete(byte id = 0)
        {
            if (!User.IsPriveleged())
            {
                return View("AccessDenied");
            }

            Rank rank = db.Ranks.Single(r => r.Id == id);
            if (rank == null)
            {
                return HttpNotFound();
            }
            return View(rank);
        }

        //
        // POST: /Rank/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(byte id)
        {
            if (!User.IsPriveleged())
            {
                return View("AccessDenied");
            }

            Rank rank = db.Ranks.Single(r => r.Id == id);
            db.Ranks.DeleteObject(rank);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}