﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Cadet.PhysicalAccounting.Web.Models;

namespace Cadet.PhysicalAccounting.Web.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            var tree = FacultyTreeMaker.CurrentFacultyTree;
            ViewBag.Message = "Физическая подготовка курсантов";

            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Приложение для сбора статистики физического развития курсантов";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Обратная связь";

            return View();
        }

        public ActionResult Generate()
        {
            var testDataGenerator = new TestDataGenerator();
            testDataGenerator.FillDB();

            return View("Index");
        }

        public ActionResult Edit()
        {
            return View();
        }
    }
}
