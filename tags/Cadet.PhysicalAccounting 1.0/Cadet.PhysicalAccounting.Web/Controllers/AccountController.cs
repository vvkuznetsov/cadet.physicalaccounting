﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Transactions;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using Cadet.PhysicalAccounting.Web.Filters;
using Cadet.PhysicalAccounting.Web.Models;
using DotNetOpenAuth.AspNet;
using Microsoft.Web.WebPages.OAuth;
using WebMatrix.WebData;

namespace Cadet.PhysicalAccounting.Web.Controllers
{
    [Authorize]
    [InitializeSimpleMembership]
    public class AccountController : Controller
    {
        //
        // GET: /Account/Login

        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        //
        // POST: /Account/Login

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Login(LoginModel model, string returnUrl)
        {
            if (ModelState.IsValid && WebSecurity.Login(model.UserName, model.Password, persistCookie: model.RememberMe))
            {
                return RedirectToLocal(returnUrl);
            }

            // If we got this far, something failed, redisplay form
            ModelState.AddModelError("", "Введенное имя пользователя или пароль не правильны.");
            return View(model);
        }

        //
        // POST: /Account/LogOff

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LogOff()
        {
            WebSecurity.Logout();

            return RedirectToAction("Index", "Home");
        }

        //
        // GET: /Account/Register

        public ActionResult Register()
        {
            if (!User.IsAdmin())
            {
                return View("AccessDenied");
            }

            return View();
        }

        //
        // POST: /Account/Register

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Register(RegisterModel model)
        {
            if (!User.IsAdmin())
            {
                return View("AccessDenied");
            }

            if (ModelState.IsValid)
            {
                // Attempt to register the user
                try
                {
                    WebSecurity.CreateUserAndAccount(model.UserName, model.Password);
                    Roles.Provider.AddUsersToRoles(new[] { model.UserName }, new[] { model.Role });

                    ViewBag.StatusMessage = "Пользователь создан успешно.";
                    return View("Register");
                }
                catch (MembershipCreateUserException e)
                {
                    ModelState.AddModelError("", ErrorCodeToString(e.StatusCode));
                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //
        // GET: /Account/Delete

        public ActionResult Delete()
        {
            if (!User.IsAdmin())
            {
                return View("AccessDenied");
            }

            return View();
        }

        //
        // POST: /Account/Manage

        [HttpPost]
        public ActionResult Delete(DeleteModel model)
        {
            if (!User.IsAdmin())
            {
                return View("AccessDenied");
            }

            if (ModelState.IsValid)
            {
                if (!Membership.Provider.ValidateUser(model.UserName, model.Password))
                {
                    ViewBag.StatusMessage = "Имя пользователя или пароль не действительны";
                }
                else
                {
                    // Attempt to delete the user
                    try
                    {
                        Roles.Provider.RemoveUsersFromRoles(new[] { model.UserName }, Roles.Provider.GetRolesForUser(model.UserName));
                        Membership.Provider.DeleteUser(model.UserName, true);
                        ViewBag.StatusMessage = "Пользователь успешно удален.";
                    }
                    catch (MembershipCreateUserException e)
                    {
                        ModelState.AddModelError("", ErrorCodeToString(e.StatusCode));
                    }
                }
            }

            return View(model);
        }
        

        //
        // GET: /Account/Manage

        public ActionResult Manage(ManageMessageId? message)
        {
            ViewBag.StatusMessage =
                message == ManageMessageId.ChangePasswordSuccess ? "Ваш пароль изменен."
                : message == ManageMessageId.SetPasswordSuccess ? "Ваш пароль установлен."
                : message == ManageMessageId.RemoveLoginSuccess ? "Ваш логин удален."
                : "";
            ViewBag.ReturnUrl = Url.Action("Manage");
            return View();
        }

        //
        // POST: /Account/Manage

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Manage(LocalPasswordModel model)
        {
            ViewBag.ReturnUrl = Url.Action("Manage");

            if (ModelState.IsValid)
            {
                // ChangePassword will throw an exception rather than return false in certain failure scenarios.
                bool changePasswordSucceeded;
                try
                {
                    changePasswordSucceeded = WebSecurity.ChangePassword(User.Identity.Name, model.OldPassword,
                                                                         model.NewPassword);
                }
                catch (Exception)
                {
                    changePasswordSucceeded = false;
                }

                if (changePasswordSucceeded)
                {
                    return RedirectToAction("Manage", new {Message = ManageMessageId.ChangePasswordSuccess});
                }
                else
                {
                    ModelState.AddModelError("",
                                             "Введенный пароль не верен или новый пароль не удовлетворяет критериям.");
                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        #region Helpers
        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        public enum ManageMessageId
        {
            ChangePasswordSuccess,
            SetPasswordSuccess,
            RemoveLoginSuccess,
        }

        private static string ErrorCodeToString(MembershipCreateStatus createStatus)
        {
            // See http://go.microsoft.com/fwlink/?LinkID=177550 for
            // a full list of status codes.
            switch (createStatus)
            {
                case MembershipCreateStatus.DuplicateUserName:
                    return "Пользователь с таким именем уже существует. Пожалуйста, введите другое имя пользователя.";

                //case MembershipCreateStatus.DuplicateEmail:
                //    return "A user name for that e-mail address already exists. Please enter a different e-mail address.";

                case MembershipCreateStatus.InvalidPassword:
                    return "Введенный пароль некорректен. Пожалуйста, проверьте введенный пароль и попробуйте еще раз.";

                //case MembershipCreateStatus.InvalidEmail:
                //    return "The e-mail address provided is invalid. Пожалуйста, проверьте введенные данные и попробуйте еще раз.";

                //case MembershipCreateStatus.InvalidAnswer:
                //    return "The password retrieval answer provided is invalid. Пожалуйста, проверьте введенные данные и попробуйте еще раз.";

                //case MembershipCreateStatus.InvalidQuestion:
                //    return "The password retrieval question provided is invalid. Пожалуйста, проверьте введенные данные и попробуйте еще раз.";

                case MembershipCreateStatus.InvalidUserName:
                    return "Введенное имя пользователя не корректно. Пожалуйста, проверьте введенные данные и попробуйте еще раз.";

                case MembershipCreateStatus.ProviderError:
                    return "Авторизация завершилась с ошибкой. Пожалуйста, проверьте введенные данные и попробуйте еще раз. Если проблема сохранится, свяжитесь со своим системным администратором.";

                case MembershipCreateStatus.UserRejected:
                    return "Запрос на создание пользователя был отменен. Пожалуйста, проверьте введенные данные и попробуйте еще раз. Если проблема сохранится, свяжитесь со своим системным администратором.";

                default:
                    return "Случилась не предвиденная ошибка. Пожалуйста, проверьте введенные данные и попробуйте еще раз. Если проблема сохранится, свяжитесь со своим системным администратором.";
            }
        }
        #endregion
    }
}
