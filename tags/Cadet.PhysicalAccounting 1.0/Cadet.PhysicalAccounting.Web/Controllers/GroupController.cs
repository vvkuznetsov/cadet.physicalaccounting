﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Cadet.PhysicalAccounting.DBProxy;

namespace Cadet.PhysicalAccounting.Web.Controllers
{
    public class GroupController : Controller
    {
        private DBContext db = new DBContext();

        //
        // GET: /Group/

        public ActionResult Index(int courseId = -1)
        {
            if (!User.IsPriveleged())
            {
                return View("AccessDenied");
            }

            var course = db.Courses.Where(c=>c.Id == courseId).Include("Faculty").FirstOrDefault();
            if (course != null)
            {
                ViewBag.FacultyName = course.Faculty.Title;
                ViewBag.CourseNumber = course.Number;
                ViewBag.CourseId = courseId;
                ViewBag.FacultyId = course.FacultyId;
            }

            var groups = db.Groups.Where(g=>g.CourseId == courseId).Include("Course");
            return View(groups.ToList().OrderBy(g=>g.Number));
        }

        //
        // GET: /Group/Details/5

        public ActionResult Details(int id = 0)
        {
            if (!User.IsPriveleged())
            {
                return View("AccessDenied");
            }

            Group group = db.Groups.Single(g => g.Id == id);
            if (group == null)
            {
                return HttpNotFound();
            }
            return View(group);
        }

        //
        // GET: /Group/Create

        public ActionResult Create(int facultyId, int courseId)
        {
            if (!User.IsPriveleged())
            {
                return View("AccessDenied");
            }

            ViewBag.CourseId = new SelectList(db.Courses.Where(c=>c.FacultyId == facultyId), "Id", "Number",courseId);
            ViewBag.SemesterId = new SelectList(db.Semesters, "Id", "Number");

            ViewBag.SelectedCourseId = courseId;
            return View();
        }

        //
        // POST: /Group/Create

        [HttpPost]
        public ActionResult Create(Group group)
        {
            if (!User.IsPriveleged())
            {
                return View("AccessDenied");
            }

            if (ModelState.IsValid)
            {
                db.Groups.AddObject(group);
                db.SaveChanges();
                FacultyTreeMaker.RefreshFacultyTreeModel();
                return RedirectToAction("Index", new { courseId = group.CourseId});
            }

            ViewBag.CourseId = new SelectList(db.Courses, "Id", "Number", group.CourseId);
            ViewBag.SemesterId = new SelectList(db.Semesters, "Id", "Number", group.SemesterId);
            return View(group);
        }

        //
        // GET: /Group/Edit/5

        public ActionResult Edit(int id = 0)
        {
            if (!User.IsPriveleged())
            {
                return View("AccessDenied");
            }

            Group group = db.Groups.Single(g => g.Id == id);
            if (group == null)
            {
                return HttpNotFound();
            }
            
            ViewBag.CourseId = new SelectList(db.Courses.Where(c=>c.FacultyId == group.Course.FacultyId), "Id", "Number", group.CourseId);
            ViewBag.SemesterId = new SelectList(db.Semesters, "Id", "Number", group.SemesterId);
            return View(group);
        }

        //
        // POST: /Group/Edit/5

        [HttpPost]
        public ActionResult Edit(Group group)
        {
            if (!User.IsPriveleged())
            {
                return View("AccessDenied");
            }

            if (ModelState.IsValid)
            {
                db.Groups.Attach(group);
                db.ObjectStateManager.ChangeObjectState(group, EntityState.Modified);
                db.SaveChanges();
                FacultyTreeMaker.RefreshFacultyTreeModel();
                return RedirectToAction("Index", new { courseId = group.CourseId});
            }

            ViewBag.SemesterId = new SelectList(db.Semesters, "Id", "Number", group.SemesterId);
            ViewBag.CourseId = new SelectList(db.Courses, "Id", "Number", group.CourseId);
            return View(group);
        }

        //
        // GET: /Group/Delete/5

        public ActionResult Delete(int id = 0)
        {
            if (!User.IsPriveleged())
            {
                return View("AccessDenied");
            }

            Group group = db.Groups.Single(g => g.Id == id);
            if (group == null)
            {
                return HttpNotFound();
            }
            return View(group);
        }

        //
        // POST: /Group/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            if (!User.IsPriveleged())
            {
                return View("AccessDenied");
            }

            Group group = db.Groups.Single(g => g.Id == id);
            int cId = group.CourseId;
            foreach (var cadet in group.Cadets.ToList())
            {
                foreach (var result in cadet.Results.ToList())
                {
                    db.Results.DeleteObject(result);
                }
                db.Cadets.DeleteObject(cadet);               
            }
            db.Groups.DeleteObject(group);
            db.SaveChanges();

            FacultyTreeMaker.RefreshFacultyTreeModel();
            return RedirectToAction("Index", new { courseId = cId });
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}