﻿$(document).ready(function () {
 
    $(this).parent().children('ul.tree').toggle(300);
    
    $('label.tree-toggler').click(function () {
        $(this).parent().children('ul.tree').toggle(300);
    });
});