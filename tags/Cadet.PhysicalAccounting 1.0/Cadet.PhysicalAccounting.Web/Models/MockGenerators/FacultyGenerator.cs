﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Cadet.PhysicalAccounting.Web.Models.MockGenerators
{
    public static class FacultyGenerator
    {
        public static DBProxy.Faculty GetFaculty(int id, int number, string title)
        {
            var result = new DBProxy.Faculty();

            result.Id = id;
            result.Number = number;
            result.Title = title;
            result.Courses = new System.Data.Objects.DataClasses.EntityCollection<DBProxy.Course>();
            
            var courses = CourseGenerator.GetCourseList();
            courses.ForEach(x => result.Courses.Add(x));

            return result;
        }


        public static List<DBProxy.Faculty> GetFacultyList(int amount = 9)
        {
            var result = new List<DBProxy.Faculty>();

            var facs = new List<string>
            {
                "конструкции летательных аппаратов",
                "систем управления ракетно-космических комплексов",
                "радиоэлектронных систем космических комплексов",
                "наземной космической инфраструктуры",
                "сбора и обработки информации",
                "информационного обеспечения и вычислительной техники",
                "топогеодезического обеспечения и картографии",
                "средств ракетно-космической обороны",
                "автоматизированных систем управления войсками"
            };
                
            int fac_index = 0;
            for (int i = 0; i < amount; i++)
            {
                if (fac_index == facs.Count)
                    fac_index = 0;

     
                result.Add(GetFaculty(i,i+1,facs[fac_index]));
                fac_index++;
            }

            return result;
        }


    }
}