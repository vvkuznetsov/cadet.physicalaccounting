﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Cadet.PhysicalAccounting.Web.Models.MockGenerators
{
    public static class GroupGenerator
    {
        public static DBProxy.Group GetGroup(int id, int number)
        {
            var result = new DBProxy.Group();

            result.Id = id;
            result.Number = number;
            return result;
        }


        public static List<DBProxy.Group> GetGroupList(int amount = 3)
        {
            var result = new List<DBProxy.Group>();
                
            for (int i = 0; i < amount; i++)
            {
                result.Add(GetGroup(i, i + 1));
            }

            return result;
        }


    }
}