﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Cadet.PhysicalAccounting.DBProxy;

namespace Cadet.PhysicalAccounting.Web.Models
{
    public static class NormsTypes
    {
        static NormsTypes ()
        {
            using (var context = new DBContext())
            {
                Power = context.NormTypes.Single(norm => norm.Id == PowerId).Name;
                Agility = context.NormTypes.Single(norm => norm.Id == AgilityId).Name;
                Speed = context.NormTypes.Single(norm => norm.Id == SpeedId).Name;
                Endurance = context.NormTypes.Single(norm => norm.Id == EnduranceId).Name;
                Military = context.NormTypes.Single(norm => norm.Id == MilitaryId).Name;
            }
        }

        public const int PowerId = 1;
        public const int AgilityId = 2;
        public const int SpeedId = 3;
        public const int EnduranceId = 4;
        public const int MilitaryId = 5;

        public static readonly string Power;
        public static readonly string Agility;
        public static readonly string Speed;
        public static readonly string Endurance;
        public static readonly string Military;
    }
}